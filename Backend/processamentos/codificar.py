import hashlib


def codificar(parametro):
    parametroCriptografado = hashlib.sha256(parametro.encode()).hexdigest()
    return parametroCriptografado
