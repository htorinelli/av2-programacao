from Backend.tiposInformacao.quadrado import *
from Backend.tiposInformacao.usuario import *
from Backend.tiposInformacao.equacao2grau import *
from Backend.processamentos.codificar import *
from Backend.processamentos.calculos import *


@app.route("/registro/<string:informacao>", methods=['post'])
def incluir(informacao):
    dados = request.get_json()
    nova = None
    if informacao == 'Usuario':
        nova = Usuario(**dados)
        nova.senha = codificar(nova.senha)
        db.session.add(nova)
        db.session.commit()
        return jsonify({"resultado": "ok", "detalhes": "ok"})
    else:
        for dado in dados:
            print(dado)
        del dados['opcoes']
        try:
            if informacao == "Quadrado":
                del dados['a']
                del dados['b']
                del dados['c']
                nova = Quadrado(**dados)
                nova.area = areaQuadrado(float(nova.lado))
            elif informacao == "Equacao":
                del dados['lado']
                nova = Equacao(a=dados['a'], b= dados['b'], c = dados['c'])
                if float(nova.a) == 0:
                    raise ValueError('"a" não pode ser 0.')

                delta = (float(nova.b) ** 2) - 4 * float(nova.a) * float(nova.c)
                if delta < 0:
                    nova.raiz1 = 'irracional'
                    nova.raiz2 = 'irracional'
                else:
                    nova.raiz1 = (-float(nova.b) + delta ** (1/2)) / (2*float(nova.a))
                    nova.raiz2 = (-float(nova.b) - delta ** (1/2)) / (2*float(nova.a))
            db.session.add(nova)
            db.session.commit()

            return jsonify({"resultado": "ok", "detalhes": "ok"})
        except Exception as e:
            return jsonify({"resultado": "erro", "detalhes": str(e)})


@app.route("/listarUsuarios")
def listar():
    dados = db.session.query(Usuario).all()
    if dados:
        lista_jsons = [x.json() for x in dados]
        meujson = {"resultado": "ok"}
        meujson.update({"detalhes": lista_jsons})
        return jsonify(meujson)
    else:
        return jsonify({"resultado": "erro"})
