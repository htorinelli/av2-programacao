from Backend.config import *
from Backend.processamentos.calculos import areaQuadrado

class Quadrado(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    lado = db.Column(db.Numeric)
    area = db.Column(db.Numeric)

    def __str__(self):
        return f"Lado: {self.lado}, Área: {areaQuadrado(self.lado)}"

    def json(self):
        return {
            "id": self.id,
            "lado": self.lado,
            "area": areaQuadrado(self.lado)
        }
