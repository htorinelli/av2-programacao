from Backend.processamentos.codificar import codificar
from Backend.config import *


class Usuario(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String)
    email = db.Column(db.String)
    senha = db.Column(db.String)

    def __str__(self):
        return f"{self.nome}, {self.email}, senha: {codificar(self.senha)}"

    def json(self):
        return {
            "id": self.id,
            "nome": self.nome,
            "email": self.email,
            "senha": codificar(self.senha)
        }

