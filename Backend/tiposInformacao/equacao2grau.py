from Backend.config import *


class Equacao(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    a = db.Column(db.Numeric)
    b = db.Column(db.Numeric)
    c = db.Column(db.Numeric)
    raiz1 = db.Column(db.String)
    raiz2 = db.Column(db.String)

    def __str__(self):
        return f"S = {[self.raiz1, self.raiz2]}"

    def json(self):
        return {
            "id": self.id,
            "a": self.a,
            "b": self.b,
            "c": self.c,
            "raiz1": self.raiz1,
            "raiz2": self.raiz2
        }
