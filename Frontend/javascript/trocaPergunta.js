const formaSelect = document.querySelector('#opcoes')
formaSelect.addEventListener('change', function() {
    var formaSelecionada = formaSelect.value
    const quadrado = document.querySelector('.quadrado')
    const equacao = document.querySelector('.equacao')

    if (formaSelecionada == 'quadrado') {
        quadrado.style.display = 'block';
        equacao.style.display = 'none';
    }

    if (formaSelecionada == 'equacao') {
        quadrado.style.display = 'none';
        equacao.style.display = 'block';
 }
});


registroBotao = document.querySelector("#registrarBotao");

registroBotao.addEventListener('click', function(e) {
   e.preventDefault()
   var formaSelecionada = document.querySelector('#opcoes').value;
   console.log(formaSelecionada)
   if (formaSelecionada == 'quadrado') {
        var classe = 'Quadrado';
   }
   if (formaSelecionada == 'equacao') {
        var classe = 'Equacao';
   }

   var rota = `http://localhost:5000/registro/${classe}`;
   console.log(rota)
   var vetor_dados = $("#forma").serializeArray();

   var chave_valor = {};
   for (var i = 0; i < vetor_dados.length; i++) {
       chave_valor[vetor_dados[i]['name']] = vetor_dados[i]['value'];
   }

   var dados_json = JSON.stringify(chave_valor);
   var acao = $.ajax({
       url: rota,
       method: 'POST',
       dataType: 'json',
       contentType: 'application/json',
       data: dados_json
   });
   acao.done(function (retorno) {
       try {
           if (retorno.resultado == "ok") {
               alert("registro inserido!");
               $("#mensagem").text("Registro incluído");
               $("#mensagem").fadeOut(3000, function () {
                   location.reload(true);
               });
           } else {
               alert("Deu algum erro :-( " + retorno.detalhes);
           }
       } catch (error) {
           alert("Erro ao tentar fazer o ajax: " + error +
               "\nResposta da ação: " + retorno);
       }
   });

   acao.fail(function (jqXHR, textStatus) {
       mensagem = encontrarErro(jqXHR, textStatus, rota);
       alert("Erro na chamada ajax: " + mensagem);
   });
});