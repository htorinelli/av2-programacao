$(document).on("click", "#registrarBotao", function () {
                var rota = 'http://localhost:5000/registro/Usuario';
                var vetor_dados = $("#registroForm").serializeArray();


                var chave_valor = {};
                for (var i = 0; i < vetor_dados.length; i++) {
                    chave_valor[vetor_dados[i]['name']] = vetor_dados[i]['value'];
                }

                var dados_json = JSON.stringify(chave_valor);
                var acao = $.ajax({
                    url: rota,
                    method: 'POST',
                    dataType: 'json', // os dados são recebidos no formato json,
                    contentType: 'application/json', // os dados serão enviados em json
                    data: dados_json
                });
                acao.done(function (retorno) {
                    try {
                        if (retorno.resultado == "ok") {

                            $("#mensagem").text("Registro incluído");
                            $("#mensagem").fadeOut(3000, function () {

                                location.reload(true);
                            });
                        } else {
                            alert("Deu algum erro :-( " + retorno.detalhes);
                        }
                    } catch (error) { // se algo der errado...
                        alert("Erro ao tentar fazer o ajax: " + error +
                            "\nResposta da ação: " + retorno);
                    }
                });

                // se a chamada der erro
                acao.fail(function (jqXHR, textStatus) {
                    mensagem = encontrarErro(jqXHR, textStatus, rota);
                    alert("Erro na chamada ajax: " + mensagem);
                });

            });


/*FUNÇÃO UTILITÁRIA*/

function encontrarErro(jqXHR, textStatus, rota) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Não foi possível conectar, ' +
                        'verifique se o endereço do backend está certo' +
                        ' e se o backend está rodando.';
                } else if (jqXHR.status == 404) {
                    msg = 'A URL informada não foi encontrada no ' +
                        'servidor [erro 404]: ' + rota;
                } else if (jqXHR.status == 500) {
                    msg = 'Erro interno do servidor [erro 500], ' +
                        'verifique nos logs do servidor';
                } else if (textStatus === 'parsererror') {
                    msg = 'Falha ao decodificar o resultado json';
                } else if (textStatus === 'timeout') {
                    msg = 'Tempo excessivo de conexão, estourou o limite (timeout)';
                } else if (textStatus === 'abort') {
                    msg = 'Requisição abortada (abort)';
                } else {
                    msg = 'Erro desconhecido: ' + jqXHR.responseText;
                }
                return msg;
            };