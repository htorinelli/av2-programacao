$(document).on("click", "#loginBotao", function () {
    var rota = 'http://localhost:5000/validarLogin/Usuario';
                var vetor_dados = $("#loginForm").serializeArray();

                var chave_valor = {};
                for (var i = 0; i < vetor_dados.length; i++) {
                    chave_valor[vetor_dados[i]['name']] = vetor_dados[i]['value'];
                }

                var dados_json = JSON.stringify(chave_valor);
                var acao = $.ajax({
                    url: rota,
                    method: 'POST',
                    dataType: 'json', // os dados são recebidos no formato json,
                    contentType: 'application/json', // os dados serão enviados em json
                    data: dados_json
                });
                acao.done(function (retorno) {
                    try {
                        if (retorno.resultado == "ok") {
                            $("#mensagem").text("Registro incluído");
                            $("#mensagem").fadeOut(3000, function () {
                                location.reload(true);
                            });
                        } else {
                            alert("Deu algum erro :-( " + retorno.detalhes);
                        }
                    } catch (error) { // se algo der errado...
                        alert("Erro ao tentar fazer o ajax: " + error +
                            "\nResposta da ação: " + retorno);
                    }
                });


                acao.fail(function (jqXHR, textStatus) {
                    mensagem = encontrarErro(jqXHR, textStatus, rota);
                    alert("Erro na chamada ajax: " + mensagem);
                });

            });
